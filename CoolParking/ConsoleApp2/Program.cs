﻿using System;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;

using System.Threading;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;


namespace Project
{
    class Program
    {
        static void Main(string[] args)
        {
            TimerService timerPay = new TimerService();
            timerPay.Interval = Settings.time_pay;

            TimerService timerLog = new TimerService();
            timerLog.Interval = Settings.log_time;

            LogService log = new LogService();
            ParkingService parking = new ParkingService(timerPay, timerLog, log);

            string answerСycle;

            do
            {
                int answerMainMenu = mainMenu();
                string answerAddVehicle="";
                Console.Clear();

                switch (answerMainMenu)
                {
                    case 1:
                        Console.WriteLine("Balance " + parking.GetBalance()); 
                        break;
                    case 2:
                        var lastParkingTransactions = parking.GetLastParkingTransactions().Sum(tr => tr.Sum);
                        Console.WriteLine(lastParkingTransactions);
                        break;
                    case 3:
                        Console.WriteLine("Вiльно " + parking.GetFreePlaces() + " мiсць з " + parking.GetCapacity());
                        break;
                    case 4:
                        parking.GetVehicles();
                        break;
                    case 5:
                        {
                            
                            int idAnsw;
                            do
                            {
                                Console.WriteLine("Оберiть вид Транспортного засобу - \n(1 - Passenger Car, 2 - Track, 3 - Bus, 4 - Motorcycle)");
                                int vehicleTypeInt = Convert.ToInt32(Console.ReadLine());
                                VehicleType vehicleType = (VehicleType)vehicleTypeInt;

                                Console.Write("Введiть баланс - ");
                                decimal sumAdd = Convert.ToDecimal(Console.ReadLine());

                                Console.WriteLine("Сгенерувати id чи ввести самостійно? (Сгенерувати - 1, ввести - 2)");
                                idAnsw= Convert.ToInt32(Console.ReadLine());

                                Vehicle vechicle;

                                if (idAnsw == 1) vechicle = new Vehicle(vehicleType, sumAdd);

                                else if (idAnsw == 2)
                                {
                                    string result = Vehicle.GenerateIdByUser();
                                    if (result == "") vechicle=new Vehicle(vehicleType, sumAdd);
                                    else vechicle =new Vehicle(result, vehicleType, sumAdd);
                                    
                                }

                                else
                                {
                                    Console.WriteLine("Невірний id операцію завершено");
                                    answerAddVehicle = "y";
                                    break;
                                }

                                parking.AddVehicle(vechicle);

                                Console.WriteLine("Додати ще транспортний засiб? (y/n) ");
                                answerAddVehicle = Console.ReadLine();

                            } while (answerAddVehicle == "y");

                            break;
                        }
                    case 6:
                        Console.WriteLine("Оберiть транспортний засiб.");
                        parking.GetVehicles();
                        Console.WriteLine("Введiть Id - ");
                        string idRemove = Console.ReadLine();
                        parking.RemoveVehicle(idRemove);
                        break;
                    case 7:
                        Console.WriteLine("Оберiть транспортний засiб.");
                        parking.GetVehicles();
                        Console.WriteLine("Введіть Id - ");
                        string id = Console.ReadLine();
                        Console.WriteLine("Введіть суму поповнення - ");
                        decimal sum = Convert.ToDecimal(Console.ReadLine());
                        parking.TopUpVehicle(id, sum);
                        break;
                    case 8:
                        foreach (var b in parking.GetLastParkingTransactions())
                            Console.WriteLine($"{b.date} {b.id} {b.Sum}");
                        break;
                    case 9:
                         parking.ReadFromLog();
                       break;
                    default:
                        Console.WriteLine("Немає такої функцiї");
                        break;
                }

                if (answerAddVehicle == "n") answerСycle = "y";
                else
                {
                    Console.WriteLine("Повернутися до головного меню? (y/n)");
                    answerСycle = Console.ReadLine();
                }

            } while (answerСycle == "y");
            Console.Read();


        }
        public static int mainMenu()
        {
            Console.Clear();
            Console.WriteLine("Головне меню");
            Console.WriteLine();
            Console.WriteLine("Поточний баланс Паркiнгу \t\t\t(1)");
            Console.WriteLine("Cума зароблених коштiв за поточний перiод \t(2)");
            Console.WriteLine("Кiлькiсть вiльних мiсць на паркуваннi \t\t(3)");
            Console.WriteLine("Транспортнi засоби, що знаходяться на Паркiнгу \t(4)");
            Console.WriteLine("Поставити Транспортний засiб на Паркiнг \t(5)");
            Console.WriteLine("Забрати Транспортний засiб з Паркiнгу \t\t(6)");
            Console.WriteLine("Поповнити баланс певного Транспортного засобу \t(7)");
            Console.WriteLine("Транзакцiї Паркiнгу за поточний перiод \t\t(8)");
            Console.WriteLine("Iсторiя Транзакцiй \t\t\t\t(9)");


            Console.WriteLine();
            Console.Write("Зробiть вибiр - ");
            int answer = Convert.ToInt32(Console.ReadLine());
            return answer;
        }

        

    }
}
