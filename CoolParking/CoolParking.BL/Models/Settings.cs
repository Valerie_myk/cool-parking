﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static int start_balanse = 0;
        public static int size = 10;
        public static int time_pay = 5;
        public static int log_time = 60;

        public static Dictionary<VehicleType, decimal> types = new Dictionary<VehicleType, decimal>(4)
        {
            [VehicleType.PassengerCar] = 2,
            [VehicleType.Bus] = 3.5m,
            [VehicleType.Motorcycle] = 1,
            [VehicleType.Truck] = 5
        };

        public static decimal koef = 2.5m;

    }
}