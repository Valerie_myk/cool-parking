﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        string file = "";
        public decimal balance = Settings.start_balanse;
        public int capacity = Settings.size;
        public List<Vehicle> vehicles = new List<Vehicle>();

        private static Parking instance;

        private Parking() { }

        public static Parking getInstance()
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }

        public string FileGeneration(List<TransactionInfo> trans)
        {
            for (int i = 0; i < trans.Count; i++)
                file += $"{trans[i].date} {trans[i].id} {trans[i].Sum} \n";

            return file;
        }


        public List<TransactionInfo> Payments(List<Vehicle> vehicles)
        {
            TransactionInfo transaction = new TransactionInfo();
            List<TransactionInfo> trans = new List<TransactionInfo>();
            decimal pay = 0, amountOfPayments;

            foreach (Vehicle vehicle in vehicles)
            {
                amountOfPayments = Settings.types[vehicle.VehicleType];

                if (vehicle.Balance - amountOfPayments == 0 || vehicle.Balance - amountOfPayments > 0)
                {
                    vehicle.Balance -= amountOfPayments;
                    pay = amountOfPayments;
                }

                else if (vehicle.Balance == 0 || vehicle.Balance < 0)
                {
                    vehicle.Balance -= amountOfPayments * Settings.koef;
                    pay = amountOfPayments * Settings.koef;
                }

                else

                {
                    vehicle.Balance -= amountOfPayments;
                    pay = amountOfPayments + vehicle.Balance;
                    vehicle.Balance *= Settings.koef;
                    pay -= vehicle.Balance;
                }

                balance += pay;
                transaction.id = vehicle.Id;
                transaction.Sum = pay;
                transaction.date = DateTime.Now;

                trans.Add(transaction);
            }
            return trans;
        }

        public void Dispose()
        {
            vehicles.Clear();
            capacity = Settings.size;
            balance = Settings.start_balanse;
        }
    }
}