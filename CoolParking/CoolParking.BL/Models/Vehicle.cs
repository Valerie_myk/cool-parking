﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public readonly string Id = "";
        public readonly VehicleType VehicleType;
        public decimal Balance;


        public Vehicle(string Id, VehicleType vehicle, decimal balance)
        {
            if (balance < 0) throw new ArgumentException("Balance is < 0");
            if (Id.Length < 10) throw new ArgumentException("Id is non correct");

            this.Id = Id;
            this.VehicleType = vehicle;
            this.Balance = balance;
        }

        public Vehicle(VehicleType vehicle, decimal balance)
        {
            this.Id = GenerateRandomRegistrationPlateNumber();
            this.VehicleType = vehicle;
            this.Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rand = new Random();
            string Id = "";

            for (int i = 0; i < 10; i++)
            {
                if (i < 2 || i > 7)
                    Id += (char)rand.Next('A', 'Z' + 1);
                else if (i == 2 || i == 7)
                    Id += '-';
                else if (i > 2 && i < 7)
                    Id += rand.Next(0, 10);
            }

            return Id;

        }

        public static string GenerateIdByUser()
        {
            string id = "", forCycle = "";
            Console.WriteLine("Приклад id - FF-1234-FF");
            do
            {
                Console.WriteLine("Введіть id - ");
                id = Console.ReadLine();

                Regex regex = new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
                MatchCollection matches = regex.Matches(id);
                if (matches.Count > 0)
                    forCycle = "n";
                else
                {
                    Console.WriteLine("Id задано невірно. Спробувати ще? (y/n)");
                    forCycle = Console.ReadLine();
                    if (forCycle == "n")
                    {
                        Console.WriteLine("Id буде згенеровано автоматично");
                        id = "";
                    }
                }
            } while (forCycle == "y");

            return id;
        }
    }
}