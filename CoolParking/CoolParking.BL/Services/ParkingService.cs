﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;
using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        ILogService log = new LogService();
        ITimerService _withdrawTimer; 
        ITimerService _logTimer;
        Parking parking = Parking.getInstance();
        public List<TransactionInfo> transaction = new List<TransactionInfo>();

        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {
            this._withdrawTimer = _withdrawTimer;
            this._logTimer = _logTimer;
            log = _logService;
            StartTimers();
        }

        public void StartTimers()
        {
            _withdrawTimer.Elapsed += GetPayments;
            _withdrawTimer.Start();
            _logTimer.Elapsed += WriteLog;
            _logTimer.Start();
        }

        public void WriteLog(Object source, ElapsedEventArgs e)
        {
            if (transaction.Count > 0)
            {
                log.Write(parking.FileGeneration(transaction));
                transaction.Clear();
            }

            else log.Write($"Немає транзакцій {DateTime.Now}");

        }

        public void GetPayments(Object source, ElapsedEventArgs e)
        {
            transaction.AddRange(parking.Payments(parking.vehicles));
        }

        public decimal GetBalance()
        {
            return parking.balance;
        }

        public int GetCapacity()
        {
            return Settings.size;
        }

        public int GetFreePlaces()
        {
            return parking.capacity;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicleReadOnly = new ReadOnlyCollection<Vehicle>(parking.vehicles);

            if (vehicleReadOnly.Count == 0)
                Console.WriteLine("Немає Транспортних засобiв");

            else
            {
                foreach (Vehicle vehicle in vehicleReadOnly)
                {
                    Console.WriteLine($"{vehicle.Id} {vehicle.VehicleType} {vehicle.Balance}");
                }
            }

            return vehicleReadOnly;
        }

        public void AddVehicle(Vehicle vehicle)
        {

            foreach (Vehicle vehicleParking in parking.vehicles)
            {
                if (vehicleParking.Id == vehicle.Id) throw new ArgumentException("Duplicate vehicle");
            }

            if (parking.capacity != 0)
            {
                parking.vehicles.Add(vehicle);
                parking.capacity--;

            }

            else if (parking.capacity == 0)
            {
                throw new InvalidOperationException("Нема вiльних мiсць");
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            foreach (Vehicle vehicle in parking.vehicles)
            {
                if (vehicle.Id == vehicleId)
                {
                    if (vehicle.Balance > 0)
                    {
                        parking.vehicles.Remove(vehicle);
                        Console.WriteLine("Вилучено " + vehicle.Id + " з паркiнгу");
                        parking.capacity++;
                    }

                    else if (vehicle.Balance < 0)
                    {
                        throw new InvalidOperationException("Борг! Вилучення з паркiнгу неможливе.");
                    }
                    return;
                }

            }

            throw new ArgumentException("There is no vehicle with such id");

        }

        public void TopUpVehicle(string vehicleId, decimal cash)
        {
            if (cash < 0) throw new ArgumentException("Введений баланс менше нуля");

            foreach (Vehicle vehicle in parking.vehicles)
            {
                if (vehicle.Id == vehicleId)
                {
                    vehicle.Balance += cash;
                    Console.WriteLine("Поповнено " + vehicle.Id + " на " + cash);
                    return;
                }
            }

            throw new ArgumentException("There is no vehicle with such id");

        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            TransactionInfo[] transactions = new TransactionInfo[1];
            transactions = transaction.ToArray();
            return transactions;
        }

        public string ReadFromLog()
        {
            LogService logs = new LogService();
            return logs.Read();
        }

        public void Dispose()
        {
            parking.Dispose();
            _withdrawTimer.Stop();
            _logTimer.Stop();
            _withdrawTimer.Dispose();
            _logTimer.Dispose();

        }
    }
}