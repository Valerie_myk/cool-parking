﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using System;
using System.IO;
using CoolParking.BL.Interfaces;
using System.Text;
using System.Reflection;

namespace CoolParking.BL.Services
{

    public class LogService : ILogService
    {
        public string LogPath { get; } = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public LogService(string LogPathb)
        {
            LogPath = LogPathb;
        }

        public LogService() { }

        public void Write(string LogPathb)
        {
            using (FileStream fstream = new FileStream(LogPath, FileMode.Append))
            {
                byte[] array = Encoding.Default.GetBytes(LogPathb+"\n");
                fstream.Write(array, 0, array.Length);
            }
        }

        public string Read()
        {
            try
            {
                using (FileStream fstream = File.OpenRead($"{LogPath}"))
                {
                    byte[] array = new byte[fstream.Length];
                    fstream.Read(array, 0, array.Length);
                    string textFromFile = System.Text.Encoding.Default.GetString(array);
                    Console.WriteLine($"Текст з файла: {textFromFile}");
                    return textFromFile;
                }
            }

            catch (FileNotFoundException)
            {
                throw new InvalidOperationException("No such file");
            }
        }


    }
}
