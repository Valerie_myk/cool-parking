﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        Timer aTimer = new Timer();

        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; } = 1;
        public void Start()
        {
            aTimer.Interval = Interval * 1000;
            aTimer.Elapsed += Elapsed;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }


        public void Stop()
        {
            aTimer.Stop();
        }

        public void Dispose()
        {
            aTimer.Dispose();
        }
    }
}